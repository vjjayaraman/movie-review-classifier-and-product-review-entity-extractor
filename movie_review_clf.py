import nltk
import random

#Uses the movie_reviews dataset inherently available in the nltk
from nltk.corpus import movie_reviews

documents = [(list(movie_reviews.words(fileid)), category)
			for category in movie_reviews.categories()
			for fileid in movie_reviews.fileids(category)]

#shuffle the contents of both positive and negative reviews to
#avoid cheating
random.shuffle(documents)

all_words = []

for w in movie_reviews.words():
	all_words.append(w.lower())

# Sorts the list of all words based on the frequency from 
# higher to lower based on their number of occurences
# Indicates popularity of a word
all_words = nltk.FreqDist(all_words)

# the popular 3000 words in the data set based on the results of freq dist
word_features = list(all_words.keys())[:3000] 

# find the occurences of the popular words in the set of words
# to find out the pattern of their occurences based on probability
def find_features(document):
	words = set(document)
	features = {}
	for w in word_features:
		features[w] = (w in words)
	return features

# print find_features(movie_reviews.words('neg/cv000_29416.txt'))


featuresets = [(find_features(rev), category) for (rev,category) in documents]

print len(featuresets)

#80-20 of dataset
training_set = featuresets[:1600]
testing_set = featuresets[1600:]


clf = nltk.NaiveBayesClassifier.train(training_set)
print "Naive Bayes Algorithm Accuracy:", (nltk.classify.accuracy(clf,testing_set)) * 100

clf.show_most_informative_features(15)

